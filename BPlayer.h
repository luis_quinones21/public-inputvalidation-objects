#ifndef PLAYER_H 	
#define PLAYER_H
#include <iostream>

using namespace std;

class BPlayer {
private:
	unsigned short Number, Points, Rebs, Asts, Stls, Blocks, Fouls, Turnovers;
	float FGPercentage, ThrptPercentage, FTPercentage, FG, FGattempt, Thrpt, Thrptattempt, FT, FTattempt;
public:
	BPlayer();
	BPlayer(unsigned short number);

	void shotTaken();
	void addReb();
	void addAst();
	void addStl();
	void addBlock();
	void addFoul();
	void addTurnover();

	void remPoints();
	void remReb();
	void remAst();
	void remStl();
	void remBlock();
	void remFoul();
	void remTurnover();

	unsigned short getNumber() const;
	unsigned short getPoints() const;
	float getFGPercentage() const;
	float getThrptPercentage() const;
	float getFTPercentage() const;
	unsigned short getRebs() const;
	unsigned short getAsts() const;
	unsigned short getStls() const;
	unsigned short getBlocks() const;
	unsigned short getFouls() const;
	unsigned short getTurnovers() const;

	void showStats();

};

#endif