#include "BPlayer.h"
#include <iostream>
#include <vector>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>   
using namespace std;


void options(vector<BPlayer> &team, int &game) {
	int option;
	unsigned short player;
	bool ask = true;
	int index;
 
	do{
		cout << "Players: ";
		for(int i = 0; i < team.size(); i++) {
			cout << team[i].getNumber() << "   ";
		}
		cout << "\nChoose the player: ";
		cin >> player;
	
		
		for(int i = 0; i < team.size(); i++) {
			if(player == team[i].getNumber()) {
				index = i;
				ask = false;
			}
		}
	} while(ask);

//***********************************

	cout << "====================Menu====================\n";
	cout << "(1)  Add shot/points\n";
	cout << "(2)  Add rebound\n";
	cout << "(3)  Add assist\n";
	cout << "(4)  Add steal\n";
	cout << "(5)  Add block\n";
	cout << "(6)  Add foul\n";
	cout << "(7)  Add turnover\n\n";

	cout << "(8)  Remove shot/points\n";
	cout << "(9)  Remove rebound\n";
	cout << "(10) Remove assist\n";
	cout << "(11) Remove steal\n";
	cout << "(12) Remove block\n";
	cout << "(13) Remove foul\n";
	cout << "(14) Remove turnover\n\n";

	cout << "(15) Show stats\n";
	cout << "(16) End game\n";
	cout << "============================================\n\n";

	cout << "What is your option? ";
	cin >> option;
	cout << "\n";

	if(option == 1) { team[index].shotTaken();}
	else if(option == 2) { team[index].addReb(); }
	else if(option == 3) { team[index].addAst(); }
	else if(option == 4) { team[index].addStl(); }
	else if(option == 5) { team[index].addBlock(); }
	else if(option == 6) { team[index].addFoul(); }
	else if(option == 7) { team[index].addTurnover(); }

	else if(option == 8) { team[index].remPoints(); }
	else if(option == 9) { team[index].remReb(); }
	else if(option == 10) { team[index].remAst(); }
	else if(option == 11) { team[index].remStl(); }
	else if(option == 12) { team[index].remBlock(); }
	else if(option == 13) { team[index].remFoul(); }
	else if(option == 14) { team[index].remTurnover(); }

	else if(option == 15) { team[index].showStats(); }
	else if(option == 16) { game = 0; }
}

//------------------------------------------------------
unsigned short totalPoints(vector <BPlayer> &team){
	unsigned short total = 0;
	for(int i = 0; i < team.size(); i++){
		total = team[i].getPoints();
	}
	return total;
}
//------------------------------------------------------


void displayGameStats(vector<BPlayer> &team1, vector<BPlayer> &team2) {
	for(int i = 0; i < 42; i++) {
		cout << "*";
	}
	cout << "TEAM 1";
	for(int i = 0; i < 42; i++) {
		cout << "*";
	}


	cout << "\nPlayer\tPoints\tFG%\tThrpt%\tFT%\tRebs\tAsts\tStls\tBlocks\tFouls\tTurnovers\n";

	for(int i = 0; i < 90; i++) {
		cout << "-";
	}

	for(int i = 0; i < team1.size(); i++) {
		cout << "\n" << team1[i].getNumber() << "\t" << team1[i].getPoints() << "\t" << team1[i].getFGPercentage() << "\t" << team1[i].getThrptPercentage() 
			<< "\t" << team1[i].getFTPercentage() << "\t" << team1[i].getRebs() << "\t" << team1[i].getAsts() << "\t" << team1[i].getStls() << "\t" 
			<<  team1[i].getBlocks() << "\t" << team1[i].getFouls() << "\t" << team1[i].getTurnovers() << "\n\n";
	}

	for(int i = 0; i < 42; i++) {
		cout << "*";
	}
	cout << "TEAM 2";
	for(int i = 0; i < 42; i++) {
		cout << "*";
	}


	cout << "\nPlayer\tPoints\tFG%\tThrpt%\tFT%\tRebs\tAsts\tStls\tBlocks\tFouls\tTurnovers\n";

	for(int i = 0; i < 90; i++) {
		cout << "-";
	}

	for(int i = 0; i < team2.size(); i++) {
		cout << "\n" << team2[i].getNumber() << "\t" << team2[i].getPoints() << "\t" << team2[i].getFGPercentage() << "\t" << team2[i].getThrptPercentage() 
			<< "\t" << team2[i].getFTPercentage() << "\t" << team2[i].getRebs() << "\t" << team2[i].getAsts() << "\t" << team2[i].getStls() << "\t" 
			<<  team2[i].getBlocks() << "\t" << team2[i].getFouls() << "\t" << team2[i].getTurnovers() << "\n\n";
	}
}


int main() {
	srand(time(NULL));
	vector<BPlayer> team1, team2;
	unsigned short totalTeam1, totalTeam2;
	int players;

	cout << "How many players are in each team? ";
	cin >> players;

// Player numbers: Team 1
/********************************************
 Remove code here so that numbers can repeat
*********************************************/
	int count = 0;
	for(int i = 0; i < players; i++) {
		unsigned short number = rand() % 51;
		for(int j = 0; j < count; j++) {
			if(number == team1[i].getNumber()) {
				number = rand() % 51;
			}
		}
		team1.push_back(BPlayer(number));
	}

// Player numbers: Team 2
/********************************************
 Remove code here so that numbers can repeat
*********************************************/
	count = 0;
	for(int i = 0; i < players; i++) {
		unsigned short number = rand() % 51;
		for(int j = 0; j < count; j++) {
			if(number == team2[i].getNumber()) {
				number = rand() % 51;
			}
		}
		team2.push_back(BPlayer(number)); 
	}

	int game = 1;
	while(game) {
		int team;

		cout << "Choose a team (1 or 2): ";
		cin >> team;

		if(team == 1){
			options(team1, game);	
		}
		else{
			options(team2, game);
		}	 		 	  	                                          
	
	}
	cout << "The game has ended.\n\n";

	displayGameStats(team1, team2);

	totalTeam1 = totalPoints(team1);
	totalTeam2 = totalPoints(team2);
	
	if(totalTeam1 > totalTeam2) {
		cout << totalTeam1 << " - " << totalTeam2 << " TEAM 1 HAS WON!\n\n";
	}
	else if(totalTeam1 < totalTeam2) {
		cout << totalTeam1 << " - " << totalTeam2 << " TEAM 2 HAS WON!\n\n";
	}
	else {
		cout << totalTeam1 << " - " << totalTeam2 << " IT'S A TIE!\n\n";
	}

	return 0;
}