[English](#markdown-header-input-validation-in-objectives) | [Español](#markdown-header-validacion-de-entrada-en-objetos)

#Validación de entrada en objetos

##Objetivos:

1. Aprender la importancia de validar toda información entrada por un usuario
2. Aprender la importancia de crear variables privadas

##Introducción

La validación de entradas es escencial en el desarrollo de aplicaciones seguras ya que es la primera línea de defensa de todas las aplicaciones.  La gran mayoría de las vulnerabilidades en aplicaciones es debido a pobre validación de las entradas de las aplicaciones. Ejemplo de vulnerabilidades explotadas por pobre validación de entradas lo son:

1. Desbordamiento del Búfer 
2. Cross Site Scripting
3. Inyección de SQL, XML u otros lenguajes
4. Recorrido de directorios
5. Acceso a archivos privados

Los atacantes utilizan errores en la validación de entradas para acceder a información privilegiada, para acceder a los sistemas, o para causar negación de servicios.

Las clases pueden tener miembros públicos y miembros privados. En este laboratorio vas a aprender la importancia de crear un sistema de validación en los funciones públicas del  objeto. Es importante proteger las variables privadas o el programa puede comportarse de forma anormal. Vas a implementar el sistema de validación en la clase `BPlayer`.

---

Vas a estar editando un programa que mantiene las estadísticas de un juego de baloncesto.
Para entender el programa necesitas aprender el sistema de puntos.

* Tiro libre - cuenta 1 punto
* Field goal - cuenta 2 puntos
* Tiro de tres - cuenta 3 puntos
* Rebotes - añadir 1
* Robos - añadir 1
* Tapones -añadir 1
* Asistencias - añadir 1
* Faltas - añadir 1
* Pérdidas de balón - añadir 1

Notar que estas variables están declaradas como privadas y son declaradas como `unsigned short`.
Recuerda que `unsigned short` no tiene números negativos.

El programa le deja al usuario añadir o remover estadísticas. Tienes que implementar un sistema en los `setters` que validen si el usuario removió algo incorrectamente. Por ejemplo, si un jugador tiene 0 asistencias y el usuario le remueve una asistencia, el `setter` va a tener que verificar y avisar al usuario.

**Instrucciones**

1. En `BPlayer.cpp`, añada validación de entrada en:

	* `void remPoints()`
	* `void remReb()`
	* `void remAst()`
	* `void remStl()`
	* `void remBlock()`
	* `void remFoul()`
	* `void remTurnover()`

2. Corre y verifica que funciona.
3. En `BPlaterClient.cpp`,

	* En la función `void options(vector<BPlayer> &team, int &game)`, añade validación de entrada
	* Valida que el usuario nunca entre números negativos
	* Valida que el usuario seleccione un equipo entre 1 y 15 jugadores.

---
---
---

[English](#markdown-header-input-validation-in-objectives) | [Español](#markdown-header-validacion-de-entrada-en-objetos)

#Input Validation in Objects

##Objectives:

1. Learn the importance of input validation
2. Learn the importance of creating private variables.

##Introduction

Input validation is esential in the development of secure applications because it is the first line of defense of every application.  The vast majority of vulnerabilities in applications is due to poor input validation of the applications.  Example of vulnerabilities explited by poor input validation are:

1. Buffer overflows
2. Cross Site Scripting
3. SQL, XML or other languages injection
4. Directory Traversal
5. Access to private files

The attackers use errors in input validation to gain access to priviledged information, to gain access to systems, or to cause denial of services.

Classes can use public and private member variables. In this lab you will learn the importance of creating a validation system in the public functions of the object. It is importance to protect the private variables or the program can behave abnormally. You will implement the validating system in the class `BPlayer`. 

---

The program that you will be editing is a Basketball Statistics Tracker.
To understand the program you will need to learn basketball point system.

* Free throw - counts 1 point
* Field goal - counts 2 points
* Three point - counts 3 points
* Rebounds - add 1
* Assist - add 1
* Steals - add 1
* Blocks - add 1
* Fouls - add 1
* Turnovers - add 1

Note that these variables are declared private, and they are unsigned short.
Remember that unsigned short does not have negative numbers.

The program lets the user add or remove stats. You will need to implement a system in the setters that validates if the user wrongly removes stats. For example, if the player has 0 assist, and the user removes an assist, the setter will need to verify and tell the user.

**Instructions**

1. In `BPlayer.cpp`, add input validation in:

	* `void remPoints()`
	* `void remReb()`
	* `void remAst()`
	* `void remStl()`
	* `void remBlock()`
	* `void remFoul()`
	* `void remTurnover()`

2. Run and verify that it works.
3. In `BPlayerClient.cpp`,

	* In the function `void options(vector<BPlayer> &team, int &game)`, add input validation
	* Validate that the user never enters negative numbers
	* Validate that the user selects a team between 1 and 15 players.

---

##References:

 [1] http://cis1.towson.edu/~cssecinj/modules/cs0/input-validation-cs0-c/