#include "BPlayer.h"
#include <iostream>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>    

using namespace std;


/*====================================
			Constructor
====================================*/
BPlayer::BPlayer() {
	srand(time(NULL));
	Number = rand() % 51;
	FG = FGattempt = FGPercentage = Thrpt = Thrptattempt = ThrptPercentage = 
	FT = FTattempt = FTPercentage = Points = Rebs = Asts = Stls = Blocks = 
	Fouls = Turnovers = 0;
}

BPlayer::BPlayer(unsigned short number) {
	Number = number;
	FG = FGattempt = FGPercentage = Thrpt = Thrptattempt = ThrptPercentage = 
	FT = FTattempt = FTPercentage = Points = Rebs = Asts = Stls = Blocks = 
	Fouls = Turnovers = 0;
}


/*====================================
				Adders
====================================*/

void BPlayer::shotTaken() {
	int option;
	cout << "FG Made (1) FG Missed (2) 3pt Made (3) 3pt Missed (4)\n"
		 << "FT Made (5) FT Missed (6) None (7)\n";
	cin >> option;
	
	if(option == 1){
		FG++;
		FGattempt++;
		Points += 2;
	}
	else if(option == 2){
		FGattempt++;
	}
	else if(option == 3){
		Thrpt++;
		Thrptattempt++;
		Points += 3;
	}
	else if(option == 4){
		Thrptattempt++;
	}
	else if(option == 5){
		FT++;
		FTattempt++;
		Points++;
	}
	else if(option == 6){
		FTattempt++;
	}
	else{
		return;
	}
}

void BPlayer::addReb() { Rebs++; }

void BPlayer::addAst() { Asts++; }

void BPlayer::addStl() { Stls++; }

void BPlayer::addBlock() { Blocks++; }

void BPlayer::addFoul() { Fouls++; }

void BPlayer::addTurnover() { Turnovers++; }


/*===================================
			Subtractors
===================================*/

/*****************************************************
Remover los ifs que validan si ya estan en 0 los stats
*****************************************************/

void BPlayer::remPoints() {
	int option;
	cout << "Remove FG (1) Remove 3pt (2) Remove FT (3) None (4)\n";
	cin >> option;
	
	if(Points == 0) {
		cout << "The player doesn't have any points to remove\n\n";
	}
	 if(option == 1){
		FG--;
		FGattempt--;
		Points -= 2;
	}
	else if(option == 2){
		Thrpt--;
		Thrptattempt--;
		Points -= 3;
	}
	else if(option == 3){
		FT--;
		FTattempt--;
		Points--;
	}
	else {
		return;
	}
}

void BPlayer::remReb() { 
	if(Rebs == 0) {
		cout << "The player doesn't have any rebounds to remove\n\n";
	}
	else {
		Rebs--;
	} 
}

void BPlayer::remAst() { 
	if(Asts == 0) {
		cout << "The player doesn't have any assists to remove\n\n";
	}
	else {
		Asts--; 
	}
}

void BPlayer::remStl() { 
	if(Stls == 0) {
		cout << "The player doesn't have any steals to remove\n\n";
	}
	else {
		Stls--; 
	}
}

void BPlayer::remBlock() { 
	if(Blocks == 0) {
		cout << "The player doesn't have any blocks to remove\n\n";
	}
	else {
		Blocks--; 
	}
}

void BPlayer::remFoul() { 
	if(Fouls == 0) {
		cout << "The player doesn't have any fouls to remove\n\n";
	}
	else {
		Fouls--; 
	}
}

void BPlayer::remTurnover() { 
	if(Turnovers == 0) {
		cout << "The player doesn't have any turnovers to remove\n\n";
	}
	else {
		Turnovers--; 
	}
}


/*==================================================
					Getters
==================================================*/
unsigned short BPlayer::getNumber() const { return Number; }

unsigned short BPlayer::getPoints() const { return Points; }

float BPlayer::getFGPercentage() const { 
	if(FG == 0 && FGattempt == 0) { 
		return 0; 
	}
	else { 
		return FG / FGattempt * 100; 
	}
}

float BPlayer::getThrptPercentage() const { 
	if(Thrpt == 0 && Thrptattempt == 0) { 
		return 0; 
	}
	else { 
		return Thrpt / Thrptattempt * 100; 
	}
}

float BPlayer::getFTPercentage() const {
	if(FT == 0 && FTattempt == 0) { 
		return 0; 
	}
	else { 
		return FT / FTattempt * 100; 
	}
}

unsigned short BPlayer::getRebs() const { return Rebs; }

unsigned short BPlayer::getAsts() const { return Asts; }

unsigned short BPlayer::getStls() const { return Stls; }

unsigned short BPlayer::getBlocks() const { return Blocks; }

unsigned short BPlayer::getFouls() const { return Fouls; }

unsigned short BPlayer::getTurnovers() const { return Turnovers; }


void BPlayer::showStats() {
	cout << "Points\tFG%\tThrpt%\tFT%\tRebounds\n";
	
	for(int i = 0; i < 44; i++) {
		cout << "-";
	}
	
	cout << "\n" << Points << "\t" << getFGPercentage() << "\t"
		 <<  getThrptPercentage() << "\t" << getFTPercentage() << "\t"
		 << Rebs << "\n";
	
	for(int i = 0; i < 44; i++) {
		cout << "=";
	}

	cout << "\nAssists\tSteals\tBlocks\tFouls\tTurnovers\n";
	
	for(int i = 0; i < 44; i++) {
		cout << "-";
	}

	cout << "\n" << Asts << "\t" << Stls << "\t"
		 <<  Blocks << "\t" << Fouls << "\t"
		 << Turnovers << "\n";
}